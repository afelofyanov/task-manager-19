package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeTaskStatusById(String id, Status status);

    Project changeTaskStatusByIndex(Integer index, Status status);
}
