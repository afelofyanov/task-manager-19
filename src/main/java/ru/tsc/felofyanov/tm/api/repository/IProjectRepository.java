package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);
}
