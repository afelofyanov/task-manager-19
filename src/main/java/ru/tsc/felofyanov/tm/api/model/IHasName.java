package ru.tsc.felofyanov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);
}
