package ru.tsc.felofyanov.tm.command.system;

import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ShowArgumentCommand extends AbstractSystemCommand {
    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public String getArgument() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "Show argument list.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }
}
