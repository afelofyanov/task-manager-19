package ru.tsc.felofyanov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Logout of current user";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        getServiceLocator().getAuthService().logout();
    }
}
