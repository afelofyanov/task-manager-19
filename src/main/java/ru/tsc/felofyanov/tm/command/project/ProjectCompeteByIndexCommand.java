package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class ProjectCompeteByIndexCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Complete project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getServiceLocator().getProjectService().changeTaskStatusByIndex(index, Status.COMPLETED);
    }
}
