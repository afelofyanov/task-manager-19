package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Complete task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");

        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getServiceLocator().getTaskService().changeTaskStatusByIndex(index, Status.COMPLETED);
    }
}
