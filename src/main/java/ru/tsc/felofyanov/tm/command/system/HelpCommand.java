package ru.tsc.felofyanov.tm.command.system;

import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Show terminal commands info.";
    }

    @Override
    public void execute() {
        System.out.println("[Help]");
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command);
    }
}
