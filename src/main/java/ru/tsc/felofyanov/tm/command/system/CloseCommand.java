package ru.tsc.felofyanov.tm.command.system;

public final class CloseCommand extends AbstractSystemCommand {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.out.println("Completing the task-manager...");
        System.exit(0);
    }
}
