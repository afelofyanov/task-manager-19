package ru.tsc.felofyanov.tm.repository;

import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.model.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }
}
